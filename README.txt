SugarCRM/SuiteCRM Customer Portal module for Drupal 7.x.
=======================



Introduction
-------------

SugarPort - SugarCRM/SuiteCRM Customer Portal for Drupal is a free add-on for
Drupal that lets Drupal website owners add a customer portal in their website
with SugarCRM/SuiteCRM as the backend framework and Drupal as the front-end
interface. With this portal add-on your customers will be able to log into
their accounts and create support or complaint cases that they want to be
addressed.(Max limit of case management is 5) Through the cases module of your
SugarCRM/SuiteCRM system you will be able to get details of all the open cases
and address them as per their priority.


Features & Workflow
A record will be created in contact's module of SugarCRM/SuiteCRM when a user
registers from portal. Whenever a new user registers from the portal a new
record will be created in the contacts module of SugarCRM/SuiteCRM.
Customers can see the list View  and detail view of case module and and can add
 new cases. Admin can view those cases from their SugarCRM/SuiteCRM case module.
Admin can set portal credentials from SugarCRM/SuiteCRM by defining a workflow.
Maximum Five Users are allowed to login into portal.

Add-on Benefits
With a CRM integrated portal you can provide better customer experience by
quickly following up and resolving on all open cases and support requests.
As the customers place their own request, there are less chances of ambiguity
and miscommunication.
Our Portal add-on integrates your existing SugarCRM/SuiteCRM with Drupal
systems. So you don’t have to invest anything in creating a support portal for
your customers.



Installation
-------------

1. Install this Module by unpacking it to your Drupal /sites/all/modules
directory if you're installing by hand.

2. You can also install module from Dashboard Admin menu > Modules > Install
new Module .

3. Than Upload Zip file.

4. Enable it in Admin menu > Modules > SugarCRM/SuiteCRM Customer Portal .

5. Fill all required settings on the Administer -> SugarCRM/SuiteCRM Customer
Portal page to connect CRM.

6. Module will appear in your Admin menu as 'Drupal Customer Portal' and Front
end as 'Portal'.

7. You can find a complete manual in modules folder: drupal_sugar_portal/manual
