<?php

/**
 * Sugar6 all actions will show here.
 */

/**
 * List module.
 */
function drupal_sugar_portal_bcp_list_module() {
  global $_drupal_sugar_portal_objSCP, $base_url, $_drupal_sugar_portal_template_path;
  if (is_object($_drupal_sugar_portal_objSCP)) {
    $order_by = 'case_number';
    $order = 'desc';

    $list_result = $_drupal_sugar_portal_objSCP->get_relationships(
      'Contacts',
      $_SESSION['scp_user_id'],
      'cases',
      array('id', 'name', 'case_number', 'priority', 'status', 'date_entered'),
      $where_con,
      'date_entered desc',
      0,
      '',
      '5');
    $cnt = count($list_result->entry_list);

    $html = "";
    $html .= "<form action = '" . $base_url . "/dsp-manage-page/list/?view=get_note_attchment' method = 'post'  id='download_note_id'>
      <input type='hidden' name='scp_note_id' value='' id='scp_note_id'>
      </form>";
    $html .= "
      <form action='" . $base_url . "/dsp-manage-page/list/?view=get_doc' method='post' id='doc_submit'>
      <input type='hidden' name='scp_doc_id' value='' id='scp_doc_id'>
      </form>";
    $html .= "<div class='dsp-list-title'><div class='scp-action-header-left'><h3 class='side-icon-wrapper scp-Cases-font scp-default-font'> <i class='side-icon-wrapper fa Cases'></i>CASES</h3>";
        $html .= "<a class='scp-Cases scp-btn scp-btn-default' href='javascript:void(0);' onclick='bcp_module_call_add(0,\"Cases\",\"edit\",\"\",\"\",\"\");'><span class='fa fa-plus side-icon-wrapper'></span><span>ADD CASE</span></a>";
    $html .= "</div>";
    $html .= "</div>";

    if ($cnt > 0) {
      $html .= "<div class='scp-page-action-title'>View Cases </div>";
    }
    if (isset($_COOKIE['bcp_add_record']) && $_COOKIE['bcp_add_record'] != '' && !isset($_COOKIE['bcp_add_record_fail']) && $_COOKIE['bcp_add_record_fail'] == '') {
      $html .= "<span class='success' id='succid'>" . $_COOKIE['bcp_add_record'] . "</span>";
    }
    if (isset($_COOKIE['bcp_add_record_fail']) && $_COOKIE['bcp_add_record_fail'] != '') {
      $html .= "<span class='messages error dsp_error_msg'>" . $_COOKIE['bcp_add_record_fail'] . "</span>";
    }
    if ($cnt > 0) {
      $html .= "<div class='scp-table-responsive'>";
      $html .= "<table id='example' class='display scp-table scp-table-striped scp-table-bordered scp-table-hover' cellspacing='0' width='100%'>";
      include $_drupal_sugar_portal_template_path . "bcp_list_page_v6.php";
      $html .= "</table></div>";
    } else {
      $html .= "<div class='scp-table-responsive'>";
      $html .= "<strong class='acp-no-record'>" . t('No Record(s) Found.') . "</strong>";
    }
    $html .= "</div>";
    return $html;
  }
  else {
    $_drupal_sugar_portal_objSCP = 0;
    $conn_err = "Connection with CRM not successful. Please check CRM Version, URL, Username and Password.";
    setcookie('bcp_connection_error', $conn_err, time() + 60, '/');
    return "-1";
  }
}

/**
 *  For viewing details of records.
 */
function drupal_sugar_portal_bcp_view_module() {
  global $_drupal_sugar_portal_objSCP, $_drupal_sugar_portal_template_path;
  if (is_object($_drupal_sugar_portal_objSCP)) {
    $id = $_POST['id'];
    $html = '';
    $where_con = 'cases' . ".id = '{$id}'";
    $record_detail = $_drupal_sugar_portal_objSCP->get_entry_list('Cases', $where_con);
    include $_drupal_sugar_portal_template_path . 'bcp_view_page_v6.php';
    $html .= "</div>";
    echo $html;
  }
  else {
    $_drupal_sugar_portal_objSCP = 0;
    $conn_err = "Connection with CRM not successful. Please check CRM Version, URL, Username and Password.";
    setcookie('bcp_connection_error', $conn_err, time() + 60, '/');
    return "-1";
  }
}

/**
 *  Getting related module list v6.
 */
function drupal_sugar_portal_get_module_list_v6() {
  global $_drupal_sugar_portal_objSCP;
  $module_name = $_REQUEST['parent_name'];
  $select_fields = array('id', 'name');
  $record_detail = $_drupal_sugar_portal_objSCP->get_relationships('Contacts', $_SESSION['scp_user_id'], strtolower($module_name), $select_fields);
  $html = "";
  $html .= "<select id='parent_id' name='parent_id' class='scp-form-control'><option value=''></option>";
  for ($m = 0; $m < count($record_detail->entry_list); $m++) {
    $mod_id = $record_detail->entry_list[$m]->name_value_list->id->value;
    $mod_name = $record_detail->entry_list[$m]->name_value_list->name->value;
    $html .= "<option value=" . $mod_id . ">" . $mod_name . "</option>";
  }
  $html .= "</select>";
  return $html;
}

/**
 * For add/edit records layout in modules.
 */
function drupal_sugar_portal_bcp_add_module() {
  global $_drupal_sugar_portal_objSCP, $_drupal_sugar_portal_template_path;
  if (is_object($_drupal_sugar_portal_objSCP)) {
    $html = "";
    include $_drupal_sugar_portal_template_path . 'bcp_add_page_v6.php';
    return $html;
  }
  else {
    $_drupal_sugar_portal_objSCP = 0;
    $conn_err = "Connection with CRM not successful. Please check CRM Version, URL, Username and Password.";
    setcookie('bcp_connection_error', $conn_err, time() + 60, '/');
  }
}

/**
 * For saving add data.
 */
function drupal_sugar_portal_bcp_add_moduledata_call() {
  global $_drupal_sugar_portal_objSCP;
  if (is_object($_drupal_sugar_portal_objSCP)) {
    $logged_user_id = $_SESSION['scp_user_id'];
    $pass_name_arry = array(
      'name' => $_REQUEST['add-name'],
      'description' => $_REQUEST['add-description'],
      'status' => 'New',
      'priority' => $_REQUEST['add-priority'],
      'type' => $_REQUEST['add-type'],
    );
    $new_id = $_drupal_sugar_portal_objSCP->setEntry('Cases', $pass_name_arry);
    // Set relationship with contact.
    $_drupal_sugar_portal_objSCP->set_relationship('Contacts', $logged_user_id, 'cases', array($new_id));
    $view1 = 'list';
    $redirect_url = $view1 . '-Cases';
    $conn_err = "Case added successfully.";
    setcookie('bcp_add_record', $conn_err, time() + 30, '/');
    return $redirect_url;
  }
  else {
     $_drupal_sugar_portal_objSCP = 0;
    $conn_err = "Connection with CRM not successful. Please check CRM Version, URL, Username and Password.";
    setcookie('bcp_connection_error', $conn_err, time() + 60, '/');
  }
}
