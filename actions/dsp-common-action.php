<?php

/**
 * Pagination.
 */

function drupal_sugar_portal_pagination($total_record, $per_page = 10, $page = 1, $url = '?', $module_name, $order_by, $order, $view, $current_url) {
  $total = $total_record;
  $adjacents = "2";

  $prevlabel = "&lsaquo;";
  $nextlabel = "&rsaquo;";

  $page = ($page == 0 ? 1 : $page);

  $prev = $page - 1;
  $next = $page + 1;

  $lastpage = ceil($total / $per_page);

  $lpm1 = $lastpage - 1;

  $pagination = "";
  if ($lastpage > 1) {
    $pagination .= "<ul class='pagination'>";
    $pagination .= "<li class='page_info'>Page {$page} of {$lastpage}</li>";

    if ($page > 1) {
      if (($order_by != NULL) && ($order != NULL)) {
        $pagination .= "<li class='page-prev'><a href='javascript:void(0);' onclick='bcp_module_paging($prev,\"" . $module_name . "\",0,\"$order_by\",\"$order\",\"$view\",\"$current_url\");'>{$prevlabel}</a></li>";
      }
      else {
        $pagination .= "<li class='page-prev'><a href='javascript:void(0);' onclick='bcp_module_paging($prev,\"" . $module_name . "\",\"\",\"\",\"\",\"$view\",\"$current_url\");'>{$prevlabel}</a></li>";
      }
    }

    if ($lastpage < 7 + ($adjacents * 2)) {
      for ($counter = 1; $counter <= $lastpage; $counter++) {
        if ($counter == $page) {
          $pagination .= "<li><a class='current'>{$counter}</a></li>";
        }
        else {
          if (($order_by != NULL) && ($order != NULL)) {
            $pagination .= "<li><a href='javascript:void(0);' onclick='bcp_module_paging($counter,\"" . $module_name . "\",0,\"$order_by\",\"$order\",\"$view\",\"$current_url\");'>{$counter}</a></li>";
          }
          else {
            $pagination .= "<li><a href='javascript:void(0);' onclick='bcp_module_paging($counter,\"" . $module_name . "\",\"\",\"\",\"\",\"$view\",\"$current_url\");'>{$counter}</a></li>";
          }
        }
      }
    }
    elseif ($lastpage > 5 + ($adjacents * 2)) {
      if ($page < 1 + ($adjacents * 2)) {
        for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++) {
          if ($counter == $page) {
            $pagination .= "<li><a class='current'>{$counter}</a></li>";
          }
          else {
            if (($order_by != NULL) && ($order != NULL)) {
              $pagination .= "<li><a href='javascript:void(0);' onclick='bcp_module_paging($counter,\"" . $module_name . "\",0,\"$order_by\",\"$order\",\"$view\",\"$current_url\");'>{$counter}</a></li>";
            }
            else {
              $pagination .= "<li><a href='javascript:void(0);' onclick='bcp_module_paging($counter,\"" . $module_name . "\",\"\",\"\",\"\",\"$view\",\"$current_url\");'>{$counter}</a></li>";
            }
          }
        }

        if (($order_by != NULL) && ($order != NULL)) {
          $pagination .= "<li class='dot'>...</li>";
          $pagination .= "<li><a href='javascript:void(0);' onclick='bcp_module_paging($lpm1,\"" . $module_name . "\",0,\"$order_by\",\"$order\",\"$view\",\"$current_url\");'>{$lpm1}</a></li>";
          $pagination .= "<li><a href='javascript:void(0);' onclick='bcp_module_paging($lastpage,\"" . $module_name . "\",0,\"$order_by\",\"$order\",\"$view\",\"$current_url\");'>{$lastpage}</a></li>";
        }
        else {
          $pagination .= "<li class='dot'>...</li>";
          $pagination .= "<li><a href='javascript:void(0);' onclick='bcp_module_paging($lpm1,\"" . $module_name . "\",'\"\",\"\",\"\",\"$view\",\"$current_url\");'>{$lpm1}</a></li>";
          $pagination .= "<li><a href='javascript:void(0);' onclick='bcp_module_paging($lastpage,\"" . $module_name . "\",\"\",\"\",\"\",\"$view\",\"$current_url\");'>{$lastpage}</a></li>";
        }
      }
      elseif ($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2)) {
        if (($order_by != NULL) && ($order != NULL)) {
          $pagination .= "<li><a href='javascript:void(0);' onclick='bcp_module_paging(1,\"" . $module_name . "\",0,\"$order_by\",\"$order\",\"$view\",\"$current_url\");'>{1}</a></li>";
          $pagination .= "<li><a href='javascript:void(0);' onclick='bcp_module_paging(2,\"" . $module_name . "\",0,\"$order_by\",\"$order\",\"$view\",\"$current_url\");'>{2}</a></li>";
          $pagination .= "<li class='dot'>...</li>";
        }
        else {
          $pagination .= "<li><a href='javascript:void(0);' onclick='bcp_module_paging(1,\"" . $module_name . "\",\"\",\"\",\"\",\"$view\",\"$current_url\");'>{1}</a></li>";
          $pagination .= "<li><a href='javascript:void(0);' onclick='bcp_module_paging(2,\"" . $module_name . "\",\"\",\"\",\"\",\"$view\",\"$current_url\");'>{2}</a></li>";
          $pagination .= "<li class='dot'>...</li>";
        }

        for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++) {
          if ($counter == $page) {
            $pagination .= "<li><a class='current'>{$counter}</a></li>";
          }
          else {
            if (($order_by != NULL) && ($order != NULL)) {
              $pagination .= "<li><a href='javascript:void(0);' onclick='bcp_module_paging($counter,\"" . $module_name . "\",0,\"$order_by\",\"$order\",\"$view\",\"$current_url\");'>{$counter}</a></li>";
            }
            else {
              $pagination .= "<li><a href='javascript:void(0);' onclick='bcp_module_paging($counter,\"" . $module_name . "\",\"\",\"\",\"\",\"$view\",\"$current_url\");'>{$counter}</a></li>";
            }
          }
        }

        if (($order_by != NULL) && ($order != NULL)) {
          $pagination .= "<li class='dot'>..</li>";
          $pagination .= "<li><a href='javascript:void(0);' onclick='bcp_module_paging($lpm1,\"" . $module_name . "\",0,\"$order_by\",\"$order\",\"$view\",\"$current_url\");'>{$lpm1}</a></li>";
          $pagination .= "<li><a href='javascript:void(0);' onclick='bcp_module_paging($lastpage,\"" . $module_name . "\",0,\"$order_by\",\"$order\",\"$view\",\"$current_url\");'>{$lastpage}</a></li>";
        }
        else {
          $pagination .= "<li class='dot'>..</li>";
          $pagination .= "<li><a href='javascript:void(0);' onclick='bcp_module_paging($lpm1,\"" . $module_name . "\",\"\",\"\",\"\",\"$view\",\"$current_url\");'>{$lpm1}</a></li>";
          $pagination .= "<li><a href='javascript:void(0);' onclick='bcp_module_paging($lastpage,\"" . $module_name . "\",\"\",\"\",\"\",\"$view\",\"$current_url\");'>{$lastpage}</a></li>";
        }
      }
      else {
        if (($order_by != NULL) && ($order != NULL)) {
          $pagination .= "<li><a href='javascript:void(0);' onclick='bcp_module_paging(1,\"" . $module_name . "\",0,\"$order_by\",\"$order\",\"$view\",\"$current_url\");'>{1}</a></li>";
          $pagination .= "<li><a href='javascript:void(0);' onclick='bcp_module_paging(2,\"" . $module_name . "\",0,\"$order_by\",\"$order\",\"$view\",\"$current_url\");'>{2}</a></li>";
          $pagination .= "<li class='dot'>..</li>";
        }
        else {
          $pagination .= "<li><a href='javascript:void(0);' onclick='bcp_module_paging(1,\"" . $module_name . "\",\"\",\"\",\"\",\"$view\",\"$current_url\");'>{1}</a></li>";
          $pagination .= "<li><a href='javascript:void(0);' onclick='bcp_module_paging(2,\"" . $module_name . "\",\"\",\"\",\"\",\"$view\",\"$current_url\");'>{2}</a></li>";
          $pagination .= "<li class='dot'>..</li>";
        }

        for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++) {
          if ($counter == $page) {
            $pagination .= "<li><a class='current'>{$counter}</a></li>";
          }
          else {
            if (($order_by != NULL) && ($order != NULL)) {
              $pagination .= "<li><a href='javascript:void(0);' onclick='bcp_module_paging($counter,\"" . $module_name . "\",0,\"$order_by\",\"$order\",\"$view\",\"$current_url\");'>{$counter}</a></li>";
            }
            else {
              $pagination .= "<li><a href='javascript:void(0);' onclick='bcp_module_paging($counter,\"" . $module_name . "\",\"\",\"\",\"\",\"$view\",\"$current_url\");'>{$counter}</a></li>";
            }
          }
        }
      }
    }

    if ($page < $counter - 1) {
      if (($order_by != NULL) && ($order != NULL)) {
        $pagination .= "<li class='page-next'><a href='javascript:void(0);' onclick='bcp_module_paging($next,\"" . $module_name . "\",0,\"$order_by\",\"$order\",\"$view\",\"$current_url\");'>{$nextlabel}</a></li>";
      }
      else {
        $pagination .= "<li class='page-next'><a href='javascript:void(0);' onclick='bcp_module_paging($next,\"" . $module_name . "\",\"\",\"\",\"\",\"$view\",\"$current_url\");'>{$nextlabel}</a></li>";
      }
    }

    $pagination .= "</ul>";
  }

  return $pagination;
}
