<?php

/**
 * @file
 * Commom Connection checking file.
 */
global $_objSCP;

$modules = (object) ['Cases' => 'cases'];
// Get page $get_current_url = explode('?', $_SERVER['REQUEST_URI'], 2).
$get_current_url = explode('?', request_uri(), 2);
$get_page_parameter = NULL;
$current_url = $get_current_url[0];
if (!empty($get_current_url[1])) {
  $get_page_parameter = explode('&', $get_current_url[1]);
}
if (!empty($get_page_parameter[0])) {
  $get_page_parameter = $get_page_parameter[0];
}
if ($get_page_parameter != NULL) {
  $current_url .= "?" . $get_page_parameter;
}
