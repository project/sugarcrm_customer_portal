<?php

/**
 * Drupal sugar7 connection class file.
 *
 */
class DrupalSugarPortalSugarRestApiCall {

  public $base_url;
  public $username;
  public $password;
  public $access_token;
  public $user_id;
  public $user_datetime_format;

/*
 * For connection
 */
  public function __construct($base_url, $username, $password) {
    $this->base_url = $base_url;
    $this->username = $username;
    $this->password = $password;
    $login_response = $this->login();
    if (isset($login_response->access_token) && !empty($login_response->access_token)) {
      $this->access_token = $login_response->access_token;
    }
  }

  /**
   * Generic function to make cURL request.
   * @param string $url
   *   The URL route to use.
   * @param string $oauthtoken
   *   The oauth token.
   * @param string $type
   *   GET, POST, PUT, DELETE. Defaults to GET.
   * @param array $arguments
   *   Endpoint arguments.
   * @param array $encodeData
   *   Whether or not to JSON encode the data.
   * @param array $returnHeaders
   *   Whether or not to return the headers.
   *
   * @return mixed
   *   Mixed data will return
   */
  public function call($url, $oauthtoken = '', $type = 'GET', $arguments = array(), $encodeData = TRUE, $returnHeaders = FALSE) {
    $type = strtoupper($type);

    if ($type == 'GET') {
      $url .= "?" . http_build_query($arguments);
    }

    $curl_request = curl_init($url);

    if ($type == 'POST') {
      curl_setopt($curl_request, CURLOPT_POST, 1);
    }
    elseif ($type == 'PUT') {
      curl_setopt($curl_request, CURLOPT_CUSTOMREQUEST, "PUT");
    }
    elseif ($type == 'DELETE') {
      curl_setopt($curl_request, CURLOPT_CUSTOMREQUEST, "DELETE");
    }

    curl_setopt($curl_request, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
    curl_setopt($curl_request, CURLOPT_HEADER, $returnHeaders);
    curl_setopt($curl_request, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($curl_request, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl_request, CURLOPT_FOLLOWLOCATION, 0);

    if ($oauthtoken != '') {
      curl_setopt($curl_request, CURLOPT_HTTPHEADER, array(
          "Content-Type: application/json",
          "oauth-token: {$oauthtoken}"
      ));
    } else {
      curl_setopt($curl_request, CURLOPT_HTTPHEADER, array(
          "Content-Type: application/json"
      ));
    }

    if (!empty($arguments) && $type !== 'GET') {
      if ($encodeData) {
        // Encode the arguments as JSON.
        $arguments = json_encode($arguments);
      }
      curl_setopt($curl_request, CURLOPT_POSTFIELDS, $arguments);
    }

    $result = curl_exec($curl_request);
    if ($returnHeaders) {
      // Set headers from response.
      list($headers, $content) = explode("\r\n\r\n", $result, 2);
      foreach (explode("\r\n", $headers) as $header) {
        header($header);
      }
      // Return the nonheader data.
      return trim($content);
    }

    curl_close($curl_request);

    // Decode the response from JSON.
    $response = json_decode($result);

    return $response;
  }

  /*
   * Login call for connection.
   */
  public function login() {
    $url = $this->base_url . "/oauth2/token";
    $oauth2_token_arguments = array(
      "grant_type" => "password",
      "client_id" => "sugar",
      "client_secret" => "",
      "username" => $this->username,
      "password" => $this->password,
      "platform" => "base",
    );
    $oauth2_token_response = $this->call($url, '', 'POST', $oauth2_token_arguments);
    return $oauth2_token_response;
  }

  /*
   * Get UserTimzone.
   */
  public function getUserTimezone() {
    if ($this->access_token != '') {
      $filter_arguments = array(
        "filter" => array(
          array(
            "user_name" => $this->username,
          ),
        ),
        "max_num" => 1,
        "offset" => 0,
        "fields" => "id,first_name,last_name",
      );
      $url = $this->base_url . "/Users/filter";
      $filter_response = $this->call($url, $this->access_token, 'POST', $filter_arguments);
      $this->user_id = $filter_response->records[0]->id;

      $url = $this->base_url . "/getUserTimezone";
      $parameters = array(
        'module_name' => 'Users',
        'user_id' => $this->user_id,
      );
      $timezone_result = $this->call($url, $this->access_token, 'GET', $parameters);
      return $timezone_result;
    }
  }

  /*
   * Get relationship between two modules.
   */
  public function getRelationship($module_name, $module_id, $relationship_name, $fields = '', $where_condition = array(), $limit = '', $offset = '', $order_by = '') {
    if ($this->access_token != '') {
      $filter_arguments = array(
        "filter" => array(
          $where_condition,
        ),
        "offset" => $offset,
        "order_by" => $order_by,
        "max_num" => $limit,
        "fields" => $fields,
      );
      $url = "$this->base_url/{$module_name}/{$module_id}/link/{$relationship_name}";
      $link_response = $this->call($url, $this->access_token, 'GET', $filter_arguments);

      return $link_response;
    }
  }

  /*
   * Set entry.
   */
  public function setEntry($module_name, $set_entry_dataArray) {
    $url = $this->base_url;
    if (isset($set_entry_dataArray['id']) && $set_entry_dataArray['id'] != '') {
      $isUpdate = TRUE;
    } else {
      $isUpdate = FALSE;
    }
    if ($module_name == 'Contacts') {
      $set_entry_dataArray['enable_portal_c'] = '1';
    }
    if ($this->access_token != '') {
      if ($isUpdate == TRUE) {
        $url = $url . "/{$module_name}/{$set_entry_dataArray['id']}";
        unset($set_entry_dataArray['id']);
        $response = $this->call($url, $this->access_token, 'PUT', $set_entry_dataArray);
        return $response->id;
      }
      else {
        $url = $url . "/{$module_name}";
        $response = $this->call($url, $this->access_token, 'POST', $set_entry_dataArray);
        return $response->id;
      }
    }
  }

  /*
   * Set Relationship.
   */
  public function set_relationship($module, $module_id, $relate_module, $relate_id) {
    $url = $this->base_url;
    if ($this->access_token != '') {
      $nameValueList = array(
        'link_name' => $relate_module,
        'ids' => array($relate_id),  // Second module id.
        'sugar_id' => $module_id  // First module id.
      );
      $url = $url . "/{$module}/{$nameValueList['sugar_id']}/link";
      unset($nameValueList['sugar_id']);
      $response = $this->call($url, $this->access_token, 'POST', $nameValueList);
      return $response;
    }
  }

  /*
   * Get Record Detail by record id.
   */
  public function getRecordDetail($module, $record_id) {
    if ($this->access_token != '') {
      $url = $this->base_url . "/{$module}/{$record_id}";
      $response = $this->call($url, $this->access_token, 'GET');
      return $response;
    }
  }

  /*
   * Get Module Record Detail by passing query.
   */
  public function getModuleRecords($module_name, $fields = "", $limit = "", $offset = "") {
    if ($this->access_token != '') {
      $filter_arguments = array(
        "filter" => array(),
        "offset" => $offset,
        "max_num" => $limit,
        "fields" => $fields,
      );
      $module = $module_name;
      $url = $this->base_url;
      $url = $url . "/{$module}/filter";
      $response = $this->call($url, $this->access_token, 'POST', $filter_arguments);
      return $response;
    }
  }

  /*
   * Portal Login by username and password.
   */
  public function portalLogin($username, $password) {
    if ($this->access_token != '') {
          $filter_arguments = array(
            "filter" => array(
              array(
                '$and' => array(
                  array(
                    "username_c" => array(
                      '$equals' => "{$username}",
                    ),
                  ),
                  array(
                    "password_c" => array(
                      '$equals' => "{$password}",
                    ),
                  ),
                ),
              ),
            ),
            "offset" => 0,
            "fields" => "id,username_c,password_c,salutation,first_name,last_name,email1,account_id,title,phone_work,phone_mobile','phone_fax",
          );
        }
        $module = "Contacts";
        $url = $this->base_url;
        $url = $url . "/{$module}/filter";
        $response = $this->call($url, $this->access_token, 'POST', $filter_arguments);
        return $response;
  }

  /*
   * Check user information by username.
   */
  public function getUserInformationByUsername($username) {
    if ($this->access_token != '') {
      $filter_arguments = array(
        "filter" => array(
          array(
            "username_c" => array(
              '$equals' => "{$username}",
            ),
          ),
        ),
        "offset" => 0,
        "fields" => "id,username_c,password_c,email1",
      );
      $url = $this->base_url . "/Contacts/filter";
      $response = $this->call($url, $this->access_token, 'GET', $filter_arguments);
    }
    $isUser = isset($response->records[0]->username_c) ? $response->records[0]->username_c : '';
    if ($isUser == $username) {
      return $response;
    }
    else {
      return FALSE;
    }
  }

  /*
   * Check email id exists.
   */
  public function getPortalEmailExists($email1) {
    if ($this->access_token != '') {
      $filter_arguments = array(
        "filter" => array(
          array(
            "email1" => array(
              '$equals' => "{$email1}",
            ),
          ),
        ),
        "offset" => 0,
        "fields" => "id,username_c,password_c,email1",
      );
      $url = $this->base_url . "/Contacts/filter";
      $response = $this->call($url, $this->access_token, 'GET', $filter_arguments);
    }
    $isEmail = isset($response->records[0]->email1) ? $response->records[0]->email1 : '';
    if ($isEmail == $email1) {
      return $response;
    }
    else {
      return FALSE;
    }
  }

  /*
   * Get User Information by user id.
   */
  public function getPortalUserInformation($contact_id) {
    if ($this->access_token != '') {
      $url = $this->base_url . "/Contacts/" . $contact_id;
      $user_response = $this->call($url, $this->access_token, 'GET');
      return $user_response;
    }
  }

}
